var fs = require('fs');
const path = require('path')
const imageValidate = require('./validators/validate')
require('dotenv').config()

const DIR_FILES_PATH =  process.env.DIR_FILES_PATH
const DIR_CORRUPTED_PATH = process.env.DIR_CORRUPTED_PATH

const folderScanner  = async () => {
    
    //Get all the folder content
    const dir_content = await fs.readdirSync(DIR_FILES_PATH, { withFileTypes: true });

    //filter out directories
    const files= dir_content.filter(dir_content => dir_content.isFile())
    .map(dir_content => dir_content.name);

    const corrupted_files = []
    var date_today = new Date();
    let date = ("0" + date_today.getDate()).slice(-2);
    let month = ("0" + (date_today.getMonth() + 1)).slice(-2);
    let year = date_today.getFullYear();

    const report_name = year.toString().concat('_',month.toString(),'_',date.toString(),'.txt')

    files.forEach((file_name, index)=>{
        //console.log(file_name)
        if(file_name){

            const full_path = path.join(DIR_FILES_PATH,file_name)
            imageValidate(full_path, file_name).then(result =>{
                
                if(result){
                    console.log("Valid image")
                }
                else{
                    console.log("Invalid image")
                    corrupted_files.push(file_name)

                    try {
                        
                    fs.appendFile(path.join(__dirname,'/report',report_name), file_name.concat('\r\n'), function (err) {
                        if (err) throw err;
                        console.log('Saved!');
                    });
                    } catch (error) {
                        console.log(error.Message)
                    }
                }
            });

        }
    })
}

process.on('uncaughtException', (err) => {
    console.error('There was an uncaught error', err)
    process.exit(1) //mandatory (as per the Node docs)
})

module.exports = folderScanner()