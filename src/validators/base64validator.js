var fs = require('fs');
const mime = require('mime');
const datauri = require('datauri');
const FileType = require('file-type');

// function to encode data  to base64 encoded string
const file_type_validate = async(path , file_name)=> {
  //read binary data
  //const bitmap = fs.readFileSync(path);
  // convert binary data to base64 encoded string
  //var base64_string =  bitmap.toString('base64');

  //Get file mime type
  //const filemime = await mime.getType(path);

  //console.log(filemime)

  //Test suing data url
  /*await datauri(path, (err, content, meta) =>{
    console.log(meta.mimetype);
  });*/
  //get ext from the source file
  var file_ext = file_name.split('.').splice(-1)[0]
  const f_type = await FileType.fromFile(path)

  if(f_type == undefined){
      //console.log('invalid data type'.concat(file_name))
      return false
  }
  else if(file_ext != f_type.ext){
    //console.log('invalid data type by extension .'.concat(file_name)) 
    return false
  }
  //base64_string.split(",")
  return true
}

module.exports = file_type_validate